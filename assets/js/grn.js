function addGrnItem() {
  let prouduct_id = $('#product').val();
  let product = $('#product option:selected').text();
  let qty = $('#qty').val();
  let price = $('#price').val();
  let amount = $('#amount').val();
  let random = Math.random()

  let newRow = `<tr>
    <td>`+product+`<input type="hidden" name="grn_item[`+random+`][product_id]" value="`+prouduct_id+`"></td>
    <td>`+qty+`<input type="hidden" name="grn_item[`+random+`][qty]" value="`+qty+`"></td>
    <td>`+price+`<input type="hidden" name="grn_item[`+random+`][price]" value="`+price+`"></td>
    <td class="item-amount">`+amount+`</td>
    <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteRow($(this))">
    <i class="fa fa-trash"></i>
    </button></td>
  </tr>`;

  $('#grnItemTbl tbody').append(newRow);

  calTotal()

}

function deleteRow(btn) {
  btn.parent().parent().remove()
  calTotal()
}

function create() {

  data = $('#grnFrom').serializeArray();

  $.ajax({
    url: base_url+'grns/store',
    type: 'POST',
    dataType: 'json',
    data: data
  })
  .done(function() {
    window.location.href = base_url+'grns'
    console.log('OK');
  })
  .fail(function(data) {
    console.log(data);
    errorHandler(data.responseJSON)
  })

}

function update() {
  data = $('#grnFrom').serializeArray();

  $.ajax({
    url: base_url+'grns/update',
    type: 'POST',
    dataType: 'json',
    data: data
  })
  .done(function() {
    window.location.href = base_url+'grns'
    console.log('OK');
  })
  .fail(function(data) {
    console.log(data);
    errorHandler(data.responseJSON)
  })
}

function deleteGrn(id) {
  $.ajax({
    url: base_url+'grns/delete/'+id,
    type: 'POST',
    dataType: 'json',
  })
  .done(function() {
    window.location.href = base_url+'grns'
  })
  .fail(function(data) {
    errorHandler(data.responseJSON)
  })
}

function calTotal() {

  let total = 0;

  $('.item-amount').each(function(i, obj) {
    total += parseFloat($(obj).text());
  });

  $('#total').val(total)
}

function calAmount() {
  let qty = $('#qty').val();
  let price = $('#price').val();

  let amount = qty * price;

  $('#amount').val(amount)
}
